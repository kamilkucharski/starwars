import React from "react";

export default function Player({name, score}) {
    return(
        <div className="py-4">
            {name}, SCORE: {score}
        </div>
    )
}