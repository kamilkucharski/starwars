import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import ChampionPeople from './components/ChampionPeople';
import Player from './components/Player';

const client = axios.create({
  baseURL: 'https://swapi.dev/api'
})

const peopleRequest = client.get('/people')
const starshipsRequest = client.get('/starships')

function App() {

  const [state, setState] = useState({player1: {selectedPeople: null, selectedStarship: null},
                                      player2: {selectedPeople: null, selectedStarship: null}})
  const [score, setScore] = useState({player1: 0, player2: 0})
  const [winner, setWinner] = useState(null)
  const [people, setPeople] = useState(null)
  const [starships, setStarships] = useState(null)

  useEffect(() => {
    axios.all([peopleRequest, starshipsRequest]).then(axios.spread(function(people, starships){
      setPeople(people.data.results)
      setStarships(starships.data.results)
    }))
    
  },[state])

  useEffect(()=>{
    whoWinBattle()
    whoWinFinally()
  },[state.player1.selectedPeople, state.player2.selectedPeople])
  


  function whoWinFinally(){
    if(score.player1 && score.player2){
      if(score.player1 === score.player2){
        setWinner(null)
      }
      else if (score.player1 > score.player2){
      setWinner('player1')
    }
    else{
      setWinner('player2')
    }
  }

  }

  function whoWinBattle(){
    if(state.player1.selectedPeople && state.player2.selectedPeople){
    if(state.player1.selectedPeople.mass > state.player2.selectedPeople.mass){
      setScore({...score, player1: score.player1+1})
    }
    else if(state.player1.selectedPeople.mass < state.player2.selectedPeople.mass){
      setScore({...score, player2: score.player2+1})
    }
  }
  }

  function getRandomNumber(min, max){
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min)) + min
  }

  function drawChampion(character, player){
    switch(character){
      case "people":
        setState({...state, [player]: {selectedPeople: people[getRandomNumber(0,10)]}})
        break;
      
      case "starship": setState({...state, selectedStarship: starships[getRandomNumber(0,10)]})
      break;
      default:
        console.log('error in switch')
  }
  
}


  return people ? (
    <div className="App">
      STAR WARS GAME
      <div className="container">
        Winner is - {winner}
        <div className="row">
      <div className="col text-centered">
        <Player name="Player 1" score={score.player1}/>
        {state.player1.selectedPeople ? <ChampionPeople props={state.player1.selectedPeople}/> : null}
        <button type="button" className="btn btn-primary" onClick={() => drawChampion("people", "player1")}>DRAW Hero</button>
      </div>
      <div className="col text-centered">
        <Player name="Player 2" score={score.player2}/>
        {state.player2.selectedPeople ? <ChampionPeople props={state.player2.selectedPeople}/> : null}
        <button type="button" className="btn btn-primary" onClick={()=>drawChampion("people", "player2")}>DRAW Hero</button>
      </div>
      </div>
      </div>
    </div>
  ):(
    <div className="App">
      Loading...
    </div>
  )
}

export default App;
