import { render, screen } from '@testing-library/react';
import App from './App';

test('should show button', () => {
  render(<App />);
  expect(screen.getByRole('button', {name: 'DRAW Starship'}));
  // expect(linkElement).toBeInTheDocument();
});
